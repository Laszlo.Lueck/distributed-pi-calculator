// using System.Numerics;
// using DistributedPiCalculator;
// using DistributedPiCalculator.Calculation;
// using DistributedPiCalculator.Connections;
// using DistributedPiCalculator.To;
// using Moq;
//
// namespace DistributedPiCalculatorTest;
//
// public class CalculationRequestSubscriberTests
// {
//     private readonly Mock<ICalculator> _calculatorMock;
//     private readonly Mock<ICalculationResponseProducer> _producerMock;
//     private readonly CalculationRequestSubscriber _subscriber;
//
//     public CalculationRequestSubscriberTests()
//     {
//         _calculatorMock = new Mock<ICalculator>();
//         _producerMock = new Mock<ICalculationResponseProducer>();
//         _subscriber = new CalculationRequestSubscriber(_calculatorMock.Object, _producerMock.Object);
//     }
//
//     [Test]
//     public async Task OnMessageReceived_ValidMessage_CallsCalculateAsync()
//     {
//         // Arrange
//         var message = new CalculatorRequestDto
//         {
//             Id = 1,
//             Precision = 5,
//             Session = Guid.NewGuid()
//         };
//         var envConfig = new EnvConfig("TestHost");
//         var expectedResult = (value: new BigInteger(12345), eta: 500.0, session: message.Session);
//
//         _calculatorMock.Setup(c => c.CalculateAsync(message.Id, message.Precision, message.Session))
//             .ReturnsAsync(expectedResult);
//
//         // Act
//         await _subscriber.OnMessageReceived(message, envConfig);
//
//         // Assert
//         _calculatorMock.Verify(c => c.CalculateAsync(message.Id - 1, message.Precision, message.Session), Times.Once);
//     }
//
//     [Test]
//     public async Task OnMessageReceived_ValidMessage_PublishesResponse()
//     {
//         // Arrange
//         var message = new CalculatorRequestDto
//         {
//             Id = 1,
//             Precision = 5,
//             Session = Guid.NewGuid()
//         };
//         var envConfig = new EnvConfig("TestHost");
//         var calculationResult = (value: new BigInteger(12345), eta: 500.0f, session: message.Session);
//
//         _calculatorMock.Setup(c => c.CalculateAsync(message.Id - 1, message.Precision, message.Session))
//             .ReturnsAsync(calculationResult);
//
//         // Act
//         await _subscriber.OnMessageReceived(message, envConfig);
//
//         // Assert
//         _producerMock.Verify(p => p.PublishAsync(It.Is<CalculatorResponseDto>(dto =>
//             dto.Id == message.Id &&
//             dto.Result == calculationResult.value &&
//             dto.ProcessTimeMs == calculationResult.eta &&
//             dto.Session == message.Session &&
//             dto.HostName == envConfig.HostName
//         )), Times.Once);
//     }
//
//     [Test]
//     public async Task OnMessageReceived_WhenCalculatorThrowsException_DoesNotPublish()
//     {
//         // Arrange
//         var message = new CalculatorRequestDto
//         {
//             Id = 1,
//             Precision = 5,
//             Session = Guid.NewGuid()
//         };
//         var envConfig = new EnvConfig("TestHost");
//
//         _calculatorMock.Setup(c => c.CalculateAsync(message.Id - 1, message.Precision, message.Session))
//             .ThrowsAsync(new Exception("Calculation error"));
//
//         // Act & Assert
//         await Assert.ThrowsAsync<Exception>(() => _subscriber.OnMessageReceived(message, envConfig));
//
//         _producerMock.Verify(p => p.PublishAsync(It.IsAny<CalculatorResponseDto>()), Times.Never);
//     }
// }