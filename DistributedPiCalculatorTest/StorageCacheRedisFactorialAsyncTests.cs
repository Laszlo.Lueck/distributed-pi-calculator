// using System.Numerics;
// using DistributedPiCalculator;
// using LanguageExt.UnsafeValueAccess;
// using Microsoft.Extensions.Logging;
// using Moq;
// using SimpleMinioClient;
// namespace DistributedPiCalculatorTest;
//
// public class StorageCacheRedisFactorialAsyncTests
// {
//     private readonly Mock<ISimpleMinioClient> _minioClientMock;
//     private readonly StorageCacheMinioFactorialAsync _storageCache;
//
//     public StorageCacheRedisFactorialAsyncTests()
//     {
//         Mock<ILogger<StorageCacheMinioFactorialAsync>> loggerMock = new();
//         _minioClientMock = new Mock<ISimpleMinioClient>();
//         Mock<IConfigurationHandler> configurationHandlerMock = new();
//         _storageCache = new StorageCacheMinioFactorialAsync(loggerMock.Object, _minioClientMock.Object,
//             configurationHandlerMock.Object);
//     }
//
//     [Test]
//     public async Task GetItemFromCacheAsync_ShouldReturnValue_WhenKeyExists()
//     {
//         var key = "factorial:10";
//         var expectedValue = new BigInteger(3628800);
//         Stream memoryStream = new MemoryStream(expectedValue.ToByteArray());
//         var os = new OperationsResult<Stream>()
//         {
//             Data = memoryStream, ErrorMessage = null, HttpStatusCode = 200, IsSuccess = true
//         };
//
//         _minioClientMock
//             .Setup(d => d.GetObjectAsync(It.IsAny<string>(), key)).ReturnsAsync(os);
//
//         var result = await _storageCache.GetItemFromCacheAsync(key);
//         await Assert.That(result.IsSome).IsTrue();
//         var foo = result.IsSome ? result.ValueUnsafe() : [];
//
//         await Assert.That(foo).IsEquivalentTo(expectedValue.ToByteArray());
//     }
//
//     [Test]
//     public async Task GetItemFromCacheAsync_ShouldReturnNone_WhenKeyDoesNotExist()
//     {
//         var key = "factorial:20";
//         var os = new OperationsResult<Stream>()
//         {
//             Data = null, ErrorMessage = "element not found", HttpStatusCode = 404, IsSuccess = false
//         };
//
//         _minioClientMock
//             .Setup(db => db.GetObjectAsync(It.IsAny<string>(), key))
//             .ReturnsAsync(os);
//
//         var result = await _storageCache.GetItemFromCacheAsync(key);
//
//         await Assert.That(result.IsSome).IsFalse();
//     }
//
//     [Test]
//     public async Task SetItemToCacheAsync_ShouldSetKey_WhenCalled()
//     {
//         var key = "factorial:5";
//         var value = BigInteger.Parse("120");
//         var os = new OperationsResult
//         {
//             HttpStatusCode = 200, IsSuccess = true, ErrorMessage = null
//         };
//
//         _minioClientMock
//             .Setup(db => db.PutObjectAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Stream>()))
//             .ReturnsAsync(os);
//         
//         await _storageCache.SetItemToCacheAsync(key, value.ToByteArray());
//
//         _minioClientMock.Verify(db => db.PutObjectAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Stream>()),
//             Times.Once);
//     }
// }