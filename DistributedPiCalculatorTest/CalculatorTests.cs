using System.Numerics;
using DistributedPiCalculator;
using LanguageExt;
using Microsoft.Extensions.Logging;
using Moq;

namespace DistributedPiCalculatorTest;

public class CalculatorTests
{

    [Test]
    public async Task FakeTest()
    {
        var result = true;
        await Assert.That(result).IsTrue();
    }

}
//     private readonly Calculator _calculator;
//     private readonly Mock<IStorageCacheAsync<byte[]>> _cacheMock;
//     private readonly Mock<ILogger<Calculator>> _loggerMock;
//     private const string CacheSuffix = "fac_";
//
//     public CalculatorTests()
//     {
//         _cacheMock = new Mock<IStorageCacheAsync<byte[]>>();
//         _loggerMock = new Mock<ILogger<Calculator>>();
//         _calculator = new Calculator(_loggerMock.Object, _cacheMock.Object, "factorial");
//     }
//
//     [Test]
//     public async Task CalculateAsync_ValidInputs_ReturnsCorrectValue()
//     {
//         // Arrange
//         var iteratee = 2;
//         var precision = 30;
//         var session = Guid.NewGuid();
//
//         _cacheMock.Setup(c => c.GetItemFromCacheAsync(It.IsAny<string>()))
//             .ReturnsAsync(Option<byte[]>.None);
//
//         // Act
//         var (result, _, returnedSession) = await _calculator.CalculateAsync(iteratee, precision, session);
//
//         // Assert
//         _loggerMock.Verify(
//             x => x.Log(
//                 It.Is<LogLevel>(l => l == LogLevel.Information),
//                 It.IsAny<EventId>(),
//                 It.Is<It.IsAnyType>((v, t) => v.ToString() == $"calculate async {iteratee}"),
//                 It.IsAny<Exception>(),
//                 It.IsAny<Func<It.IsAnyType, Exception, string>>()!),
//             Times.Once);
//
//         await Assert.That(result).IsGreaterThan(BigInteger.Zero);
//         await Assert.That(returnedSession).IsEquivalentTo(session);
//
//         _loggerMock.Verify(
//             x => x.Log(
//                 It.Is<LogLevel>(l => l == LogLevel.Information),
//                 It.IsAny<EventId>(),
//                 It.Is<It.IsAnyType>((v, t) => v.ToString()!.Contains($"calculation of {iteratee} took ")),
//                 It.IsAny<Exception>(),
//                 It.IsAny<Func<It.IsAnyType, Exception, string>>()!),
//             Times.Once);
//     }
//
//     [Test]
//     public async Task CalculateAsync_CacheHit_ReturnsCachedValue()
//     {
//         // Arrange
//         var iteratee = 2;
//         var precision = 30;
//         var session = Guid.NewGuid();
//         var cachedFactorial = new byte[] { 1 };
//
//         _cacheMock.Setup(c => c.GetItemFromCacheAsync(It.IsAny<string>()))
//             .ReturnsAsync(cachedFactorial);
//
//         // Act
//         var (result, _, returnedSession) = await _calculator.CalculateAsync(iteratee, precision, session);
//
//         // Assert
//         await Assert.That(result).IsGreaterThan(BigInteger.Zero);
//         await Assert.That(returnedSession).IsEquivalentTo(session);
//         _cacheMock.Verify(c => c.GetItemFromCacheAsync(It.IsAny<string>()), Times.AtLeastOnce);
//         _loggerMock.Verify(
//             x => x.Log(
//                 It.Is<LogLevel>(l => l == LogLevel.Information),
//                 It.IsAny<EventId>(),
//                 It.Is<It.IsAnyType>((v, t) => v.ToString() == $"calculate async {iteratee}"),
//                 It.IsAny<Exception>(),
//                 It.IsAny<Func<It.IsAnyType, Exception, string>>()!),
//             Times.Once);
//         _loggerMock.Verify(
//             x => x.Log(
//                 It.Is<LogLevel>(l => l == LogLevel.Information),
//                 It.IsAny<EventId>(),
//                 It.Is<It.IsAnyType>((v, t) => v.ToString()!.Contains($"calculation of {iteratee} took ")),
//                 It.IsAny<Exception>(),
//                 It.IsAny<Func<It.IsAnyType, Exception, string>>()!),
//             Times.Once);
//     }
//
//     [Test]
//     public async Task Factorial_UsesCacheWhenAvailable()
//     {
//         // Arrange
//         var source = 2;
//         var cachedFactorial = new BigInteger(120).ToByteArray();
//
//         _cacheMock.Setup(c => c.GetItemFromCacheAsync($"{CacheSuffix}{source}"))
//             .ReturnsAsync(cachedFactorial);
//
//         // Act
//         await _calculator.CalculateAsync(source, 30, Guid.NewGuid());
//
//         // Assert
//         _cacheMock.Verify(c => c.GetItemFromCacheAsync($"{CacheSuffix}{source}"), Times.Once);
//         _loggerMock.Verify(
//             x => x.Log(
//                 It.Is<LogLevel>(l => l == LogLevel.Information),
//                 It.IsAny<EventId>(),
//                 It.Is<It.IsAnyType>((v, t) => v.ToString() == $"calculate async {source}"),
//                 It.IsAny<Exception>(),
//                 It.IsAny<Func<It.IsAnyType, Exception, string>>()!),
//             Times.Once);
//         _loggerMock.Verify(
//             x => x.Log(
//                 It.Is<LogLevel>(l => l == LogLevel.Information),
//                 It.IsAny<EventId>(),
//                 It.Is<It.IsAnyType>((v, t) => v.ToString()!.Contains($"calculation of {source} took ")),
//                 It.IsAny<Exception>(),
//                 It.IsAny<Func<It.IsAnyType, Exception, string>>()!),
//             Times.Once);
//     }
//
//     [Test]
//     public async Task Factorial_CalculatesWhenCacheMisses()
//     {
//         // Arrange
//         var source = 2;
//
//         _cacheMock.Setup(c => c.GetItemFromCacheAsync($"{CacheSuffix}{source}"))
//             .ReturnsAsync(Option<byte[]>.None);
//
//         // Act
//         var result = await _calculator.CalculateAsync(source, 30, Guid.NewGuid());
//
//         // Assert
//         await Assert.That(result.value).IsNotEqualTo(BigInteger.Zero);
//         _cacheMock.Verify(c => c.GetItemFromCacheAsync($"{CacheSuffix}{source}"), Times.Once);
//         _cacheMock.Verify(c => c.SetItemToCacheAsync($"{CacheSuffix}{source}", It.IsAny<byte[]>()), Times.Once);
//         _loggerMock.Verify(
//             x => x.Log(
//                 It.Is<LogLevel>(l => l == LogLevel.Information),
//                 It.IsAny<EventId>(),
//                 It.Is<It.IsAnyType>((v, t) => v.ToString() == $"calculate async {source}"),
//                 It.IsAny<Exception>(),
//                 It.IsAny<Func<It.IsAnyType, Exception, string>>()!),
//             Times.Once);
//         _loggerMock.Verify(
//             x => x.Log(
//                 It.Is<LogLevel>(l => l == LogLevel.Information),
//                 It.IsAny<EventId>(),
//                 It.Is<It.IsAnyType>((v, t) => v.ToString()!.Contains($"calculation of {source} took ")),
//                 It.IsAny<Exception>(),
//                 It.IsAny<Func<It.IsAnyType, Exception, string>>()!),
//             Times.Once);
//     }
//
//     [Test]
//     public async Task GenerateIntSequence_GeneratesCorrectSequence()
//     {
//         // Arrange
//         var start = 1;
//         var end = 5;
//
//         // Act
//         var sequence = Calculator.TestGenerateIntSequence(start, end);
//
//         // Assert
//         await Assert.That(sequence).IsEquivalentTo([1, 2, 3, 4, 5]);
//     }
//
//     [Test]
//     public async Task GenerateFactorial_And_Check_Mathematical_Correctness()
//     {
//         var testee = 15;
//         _cacheMock.Setup(c => c.GetItemFromCacheAsync($"{CacheSuffix}{testee}"))
//             .ReturnsAsync(Option<byte[]>.None);
//
//         var result = await new Calculator(_loggerMock.Object, _cacheMock.Object).TestFactorial(testee);
//         var realResult = new BigInteger(1307674368000);
//
//         await Assert.That(result).IsEqualTo(realResult);
//     }
// }