﻿using System.Numerics;
using DistributedPiCalculator;
using DistributedPiCalculator.Calculation;
using DistributedPiCalculator.Configuration;
using DistributedPiCalculator.Connections;
using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using OpenTelemetry.Exporter;
using OpenTelemetry.Logs;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;
using SimpleMinioClient;
using SimpleMinioClient.SimpleMinioClientHealthCheck;

var builder = WebApplication.CreateBuilder(args);

var rb = ResourceBuilder
    .CreateDefault()
    .AddService("DistributedPiCalculator");

builder
    .Logging
    .ClearProviders();

IConfigurationHandler configHandler = new ConfigurationHandler(builder.Configuration);

builder
    .Logging
    .AddOpenTelemetry(options =>
    {
        options.IncludeScopes = true;
        options.IncludeFormattedMessage = true;
        options.SetResourceBuilder(rb);
        options.AddOtlpExporter(o =>
        {
            o.Endpoint = new Uri(configHandler.GetValue<string>("LOGGING_HOST"));
            o.Headers = $"X-Seq-ApiKey={configHandler.GetValue<string>("LOGGING_API_KEY")}";
            o.Protocol = OtlpExportProtocol.HttpProtobuf;
        });

        if (builder.Environment.IsDevelopment())
            options.AddConsoleExporter();
    });

builder
    .Services
    .AddOpenTelemetry()
    .WithTracing(tcb =>
    {
        tcb
            .SetSampler<AlwaysOnSampler>()
            .SetResourceBuilder(rb)
            .AddSource([builder.Environment.ApplicationName])
            .AddAspNetCoreInstrumentation()
            .AddHttpClientInstrumentation()
            .AddOtlpExporter(o =>
            {
                o.Endpoint = new Uri(configHandler.GetValue<string>("TRACING_HOST"));
                o.Headers = $"X-Seq-ApiKey={configHandler.GetValue<string>("TRACING_API_KEY")}";
                o.Protocol = OtlpExportProtocol.HttpProtobuf;
            });
        if (builder.Environment.IsDevelopment())
            tcb.AddConsoleExporter();
    });

builder.Services.AddMinioFromEnvironment(configHandler);
builder.Services.AddSingleton(configHandler);
builder.Services.AddMetrics();
builder.Services.AddScoped<ICalculationResponseProducer, CalculationResponseProducer>();

builder.Services.AddSingleton<ICalculator, Calculator>();

var hn = configHandler.GetValue<string>("HOSTNAME");
builder.Services.AddSingleton(new EnvConfig(hn));

builder
    .Services
    .AddSilverback()
    .UseModel()
    .WithConnectionToMessageBroker(o => o.AddKafka())
    .AddEndpointsConfigurator<KafkaEndpointConfigurator>()
    .AddScopedSubscriber<CalculationRequestSubscriber>();

builder.Services.AddEndpointsApiExplorer();

builder
    .Services
    .AddHealthChecks()
    .AddSimpleMinioReadynessCheck(sp => sp.GetRequiredService<ISimpleMinioClient>())
    .AddKafka(config => { config.BootstrapServers = configHandler.GetValue<string>("KAFKA_URL"); },
        failureStatus: HealthStatus.Unhealthy, timeout: TimeSpan.FromSeconds(2), name: "KafkaHealthCheck");


var app = builder.Build();

app.Logger.LogInformation("running app on host {HostName}", configHandler.GetValue<string>("HOSTNAME"));


app.MapHealthChecks("/healthz", new HealthCheckOptions
{
    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
});

app.MapHealthChecks("/alive", new HealthCheckOptions
{
    Predicate = r => r.Tags.Contains("live")
});

app.Logger.StartingApp();
await app.RunAsync();

namespace DistributedPiCalculator
{
    public static partial class ApplicationLogs
    {
        [LoggerMessage(1, LogLevel.Information, "Starting the app...")]
        public static partial void StartingApp(this ILogger logger);
    }
}