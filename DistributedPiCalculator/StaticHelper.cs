using DistributedPiCalculator.Configuration;
using DistributedPiCalculator.Connections;
using DistributedPiCalculator.To;

namespace DistributedPiCalculator;

public static class StaticHelper
{
    public static Dictionary<MinioConnectionName, MinioConfiguration>
        LoadMinioConfigurationFromEnvironment(IConfigurationHandler configurationHandler)
    {
        var connections = new Dictionary<MinioConnectionName, MinioConfiguration>();

        var mainConfig = new MinioConfiguration
        {
            AccessKey = configurationHandler.GetValue<string>("MINIO_MAIN_ACCESS_KEY"),
            SecretKey = configurationHandler.GetValue<string>("MINIO_MAIN_SECRET_KEY"),
            Host = configurationHandler.GetValue<string>("MINIO_MAIN_HOST"),
        };

        var factorialConfig = new MinioConfiguration
        {
            AccessKey = configurationHandler.GetValue<string>("MINIO_FACTORIAL_ACCESS_KEY"),
            SecretKey = configurationHandler.GetValue<string>("MINIO_FACTORIAL_SECRET_KEY"),
            Host = configurationHandler.GetValue<string>("MINIO_FACTORIAL_HOST"),
        };
        
        connections[MinioConnectionName.MainConnection] =  mainConfig;
        connections[MinioConnectionName.FactorialConnection] = factorialConfig;
        
        return connections;
    }
}