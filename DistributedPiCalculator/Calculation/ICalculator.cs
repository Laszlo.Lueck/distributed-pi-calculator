using System.Numerics;

namespace DistributedPiCalculator.Calculation;

public interface ICalculator
{
    Task<(BigInteger value, double eta, Guid session)> CalculateAsync(int iteratee, int precision, Guid session);
}