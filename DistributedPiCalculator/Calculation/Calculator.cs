using System.Diagnostics;
using System.Numerics;
using DistributedPiCalculator.Connections;

namespace DistributedPiCalculator.Calculation;

public partial class Calculator(ILogger<Calculator> logger, IServiceProvider serviceProvider)
    : ICalculator
{
    private readonly IStorageCacheAsync _factorialStorage =
        serviceProvider.GetRequiredKeyedService<IStorageCacheAsync>(MinioBucketName.FactorialBucket.Name);

    private static readonly BigInteger ConstA = new(13591409);
    private static readonly BigInteger ConstB = new(545140134);
    private static readonly BigInteger ConstC = new(-640320);

    private const string CacheSuffix = "fac_";

    public async Task<(BigInteger value, double eta, Guid session)> CalculateAsync(int iteratee, int precision,
        Guid session)
    {
        logger.LogInformation("calculate async {Iteratee}", iteratee);
        var sw = Stopwatch.StartNew();
        var it3 = 3 * iteratee;
        var it6 = 6 * iteratee;

        var results = await Task.WhenAll
        (
            FactorialAsync(iteratee, _factorialStorage, logger),
            FactorialAsync(it3, _factorialStorage, logger),
            FactorialAsync(it6, _factorialStorage, logger)
        );

        var f1 = results[0];
        var f3 = results[1];
        var f6 = results[2];

        BigInteger bi = new(iteratee);
        var factor = BigInteger.Pow(10, precision);


        var ret = await Task.Run(() => factor * f6 * (ConstA + ConstB * bi) /
                                       (f3 * BigInteger.Pow(f1, 3) *
                                        BigInteger.Pow(ConstC, it3)));

        sw.Stop();
        logger.LogInformation("calculation of {Id} took {Elapsed}ms", iteratee, sw.Elapsed.TotalMilliseconds);
        return (ret, sw.Elapsed.TotalMilliseconds, session);
    }


    private static IEnumerable<int> GenerateIntSequence(int startValue, int endInclusive)
    {
        for (var i = startValue; i <= endInclusive; i++)
            yield return i;
    }


    private static readonly Func<int, IStorageCacheAsync, ILogger<Calculator>, Task<BigInteger>> FactorialAsync =
        async (source, cache, logger) =>
        {
            var entryOpt = await cache.GetItemFromCacheAsync($"{CacheSuffix}{source}");

            return await entryOpt.MatchAsync(Some: async entry =>
                {
                    logger.LogInformation("async hit for {Id}", source);
                    return await Task.Run(() => new BigInteger(entry));
                },
                None: async () =>
                {
                    var newEntry = Factorial!(source);
                    await cache.SetItemToCacheAsync($"{CacheSuffix}{source}", newEntry.ToByteArray());
                    logger.LogWarning("no hit for {Id}", source);
                    return newEntry;
                });
        };

    private static readonly Func<int, BigInteger> Factorial = source =>
        GenerateIntSequence(1, source)
            .AsParallel()
            .Aggregate(BigInteger.One, (acc, val) => acc * val);
}

public partial class Calculator
{
    public static IEnumerable<int> TestGenerateIntSequence(int start, int end)
    {
        return GenerateIntSequence(start, end);
    }

    public Task<BigInteger> TestFactorial(int source, IStorageCacheAsync cacheAsync, ILogger<Calculator> loggerInstance)
    {
        return FactorialAsync(source, cacheAsync, loggerInstance);
    }
}