using System.Text.Json.Serialization;

namespace DistributedPiCalculator.To;

public class CalculatorRequestDto
{
    [JsonPropertyName("id"), JsonInclude]
    public int Id;

    [JsonPropertyName("precision"), JsonInclude]
    public int Precision;

    [JsonPropertyName("session"), JsonInclude]
    public Guid Session;
}