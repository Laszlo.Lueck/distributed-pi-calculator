using DistributedPiCalculator.To;
using Silverback.Messaging.Publishing;

namespace DistributedPiCalculator.Connections;

public interface ICalculationResponseProducer
{
    Task PublishAsync(CalculatorResponseDto responseDto);
}

public class CalculationResponseProducer(ILogger<CalculationResponseProducer> logger, IPublisher publisher)
    : ICalculationResponseProducer
{
    public async Task PublishAsync(CalculatorResponseDto responseDto)
    {
        try
        {
            logger.LogInformation("send response to publish for id {Id}", responseDto.Id);
            await publisher.PublishAsync<CalculatorResponseDto>(responseDto);
        }
        catch (Exception e)
        {
            logger.LogError(e, "an error occured");
        }
    }
}