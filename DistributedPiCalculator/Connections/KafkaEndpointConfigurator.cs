using Confluent.Kafka;
using DistributedPiCalculator.Configuration;
using DistributedPiCalculator.To;
using Silverback.Messaging.Configuration;
using Silverback.Messaging.Messages;

namespace DistributedPiCalculator.Connections;

public class KafkaEndpointConfigurator(IConfigurationHandler configurationHandler) : IEndpointsConfigurator
{
    public void Configure(IEndpointsConfigurationBuilder builder)
    {
        builder
            .AddKafkaEndpoints(endpoints =>
                endpoints.Configure(config => { config.BootstrapServers = configurationHandler.GetValue<string>("KAFKA_URL"); })
                    .AddInbound(endpoint =>
                        endpoint
                            .ConsumeFrom(configurationHandler.GetValue<string>("KAFKA_TOPIC_JOB_REQUESTS")).Configure(
                                config =>
                                {
                                    config.GroupId = configurationHandler.GetValue<string>("KAFKA_GROUP_ID");
                                    config.AutoOffsetReset = AutoOffsetReset.Earliest;
                                    config.MaxPollIntervalMs = 1440000;
                                })
                            .DeserializeJson(ser => ser.UseFixedType<CalculatorRequestDto>())
                            .EnableBatchProcessing(configurationHandler.GetValue<int>("KAFKA_PARALLELISM"), TimeSpan.FromMinutes(configurationHandler.GetValue<int>("KAFKA_BATCH_TIMEOUT_MINUTES")))
                            .LimitParallelism(1)
                            .ProcessAllPartitionsTogether()
                        )
                    .AddOutbound<CalculatorResponseDto>(endpoint => endpoint
                        .ProduceTo(configurationHandler.GetValue<string>("KAFKA_TOPIC_JOB_RESPONSES"))
                        .SerializeAsJson(config => config.UseFixedType<CalculatorResponseDto>())
                    )
            );
    }
}