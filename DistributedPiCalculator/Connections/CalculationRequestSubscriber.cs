using DistributedPiCalculator.Calculation;
using DistributedPiCalculator.Configuration;
using DistributedPiCalculator.To;

namespace DistributedPiCalculator.Connections;

public class CalculationRequestSubscriber(
    ILogger<CalculationRequestSubscriber> logger,
    ICalculator calculator,
    ICalculationResponseProducer producer,
    IServiceProvider serviceProvider,
    IConfigurationHandler configurationHandler)
{
    private readonly IStorageCacheAsync _factorialStorage =
        serviceProvider.GetRequiredKeyedService<IStorageCacheAsync>(MinioBucketName.PartBucket.Name);

    private readonly int _parallelism = configurationHandler.GetValue<int>("KAFKA_PARALLELISM");

     public async Task OnBatchReceivedAsync(IAsyncEnumerable<CalculatorRequestDto> messages, EnvConfig envConfig)
     {
         await Parallel.ForEachAsync(messages, new ParallelOptions{MaxDegreeOfParallelism = _parallelism}, async (message, _) =>
         {
             var storageKey = $"{message.Id}-{message.Precision}";
             var iteratee = message.Id - 1;
    
             var keyExistAsync = await _factorialStorage.IsKeyExistAsync(storageKey);
             if (keyExistAsync)
             {
                 logger.LogInformation("key {Key} already exist", storageKey);
                 return;
             }
    
             logger.LogInformation("calculate value for iteratee {Key} with precision {Precision}", iteratee, message.Precision);
             var calculationResponse = await calculator.CalculateAsync(iteratee, message.Precision, message.Session);
    
             await _factorialStorage.SetItemToCacheAsync(storageKey, calculationResponse.value.ToByteArray());
    
             var ret = new CalculatorResponseDto
             (
                 Id: message.Id,
                 ProcessTimeMs: calculationResponse.eta,
                 Session: calculationResponse.session,
                 HostName: envConfig.HostName,
                 Precision: message.Precision,
                 LongLength: calculationResponse.value.ToByteArray().LongLength
             );
    
             await producer.PublishAsync(ret);
         });
    }
}