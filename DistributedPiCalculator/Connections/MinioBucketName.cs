namespace DistributedPiCalculator.Connections;

public class MinioBucketName
{
    public static readonly MinioBucketName PartBucket = new("PartBucket", GetValue("MINIO_CALCULATION_BUCKET_PARTS"), MinioConnectionName.MainConnection);

    public static readonly MinioBucketName FactorialBucket = new("FactorialBucket", GetValue("MINIO_CALCULATION_BUCKET_FACTORIALS"), MinioConnectionName.FactorialConnection);

    public string Name { get; }
    public string Path { get; }
    
    public MinioConnectionName ConnectionName { get; }

    private MinioBucketName(string name, string path, MinioConnectionName connectionName)
    {
        Name = name;
        Path = path;
        ConnectionName = connectionName;
    }

    private static string GetValue(string key)
    {
        return Environment.GetEnvironmentVariable(key) ??
               throw new ArgumentNullException($"value for key {key} cannot be found in configuration");
    }

    public override string ToString() => Path;
}