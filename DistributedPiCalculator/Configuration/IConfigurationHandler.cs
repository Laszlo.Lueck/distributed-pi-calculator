namespace DistributedPiCalculator.Configuration;

public interface IConfigurationHandler
{
    T GetValue<T>(string key);
}